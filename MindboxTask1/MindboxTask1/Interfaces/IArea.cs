﻿namespace MindboxTask1
{
    public interface IArea
    {
        public double Area { get; }
    }
}
