﻿namespace MindboxTask1
{
    public class Circle : IArea
    {
        private readonly double _radius;

        public double Radius
        {
            get { return _radius; }
            init
            {
                if (value > 0)
                    _radius = value;
                else
                    throw new ArgumentOutOfRangeException("Radius should be greater than zero.");
            }
        }

        public double Area => Math.PI * _radius * _radius;

        public Circle(double radius)
        {
            Radius = radius;
        }
    }
}