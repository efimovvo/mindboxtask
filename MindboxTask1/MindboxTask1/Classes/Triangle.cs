﻿namespace MindboxTask1
{
    public class Triangle : IArea
    {
        private readonly List<double > _sides;
        private double _halfPerimeter => (Sides[0] + Sides[1] + Sides[2]) / 2;

        public List<double> Sides
        {
            get { return _sides; }
            init
            {
                if (value is null || value.Count != 3)
                    throw new ArgumentException("Triangle should contain exectly 3 sides.");
                foreach (var side in value)
                {
                    if (side <= 0)
                        throw new ArgumentOutOfRangeException("Side length should be greater than zero.");
                }
                _sides = value;
            }
        }

        public double Area => Math.Pow
            (_halfPerimeter
                * (_halfPerimeter - Sides[0])
                * (_halfPerimeter - Sides[1])
                * (_halfPerimeter - Sides[2])
                , 0.5);

        public Triangle(List<double> sides)
        {
            if (IsCorrectTriangleSideSet(sides))
            {
                Sides = sides;
            }
            else
            {
                throw new ArgumentOutOfRangeException("Two sides length sum should be greater than length of third one.");
            }
        }

        public bool IsRightTriangle()
        {
            for (int i = 0; i < Sides.Count; i++)
            {
                if ((Sides[i % Sides.Count] * Sides[i % Sides.Count] + Sides[(i + 1) % Sides.Count] * Sides[(i + 1) % Sides.Count])
                    .CompareTo(Sides[(i + 2) % Sides.Count] * Sides[(i + 2) % Sides.Count]) is 0)
                    return true;
            }
            return false;
        }

        private bool IsCorrectTriangleSideSet(List<double> sideLength)
        {
            for (int i = 0; i < sideLength.Count; i++)
            {
                if (sideLength[i % sideLength.Count] + sideLength[(i + 1) % sideLength.Count] < sideLength[(i + 2) % sideLength.Count])
                    return false;
            }
            return true;
        }
    }
}