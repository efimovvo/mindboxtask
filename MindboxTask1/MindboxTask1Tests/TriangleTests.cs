using MindboxTask1;
using Xunit;
using FluentAssertions;
using System;
using System.Collections.Generic;

namespace MindboxTask1Tests
{
    public class TriangleTests
    {
        [Fact]
        public void Constructor_NegativeSide_ShouldThrowArgumentOutOfRangeException()
        {
            //Arrange

            //Act
            var func = () => { var triangle = new Triangle(new List<double> { -1, 1, 1 }); };

            //Assert
            func.Should().Throw<ArgumentOutOfRangeException>();
        }

        [Fact]
        public void Constructor_TooLongSide_ShouldThrowArgumentOutOfRangeException()
        {
            //Arrange

            //Act
            var func = () => { var triangle = new Triangle(new List<double> { 100, 1, 1 }); };

            //Assert
            func.Should().Throw<ArgumentOutOfRangeException>();
        }

        [Fact]
        public void Area_ShouldReturnCorrectArea()
        {
            //Arrange
            var triangle = new Triangle(new List<double> { 3, 4, 5 });

            //Act
            var area = triangle.Area;

            //Assert
            area.Should().BeApproximately(6.0, 1e-6);
        }

        [Fact]
        public void IsRightTriangle_ShouldReturnTrue()
        {
            //Arrange
            var triangle = new Triangle(new List<double> { 3, 4, 5 });

            //Act
            var isRightTriangle = triangle.IsRightTriangle();

            //Assert
            isRightTriangle.Should().Be(true);
        }


        [Fact]
        public void IsRightTriangle_ShouldReturnFalse()
        {
            //Arrange
            var triangle = new Triangle(new List<double> { 1, 1, 1 });

            //Act
            var isRightTriangle = triangle.IsRightTriangle();

            //Assert
            isRightTriangle.Should().Be(false);
        }
    }
}