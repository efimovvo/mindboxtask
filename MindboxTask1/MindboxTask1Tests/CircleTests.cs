using MindboxTask1;
using Xunit;
using FluentAssertions;
using System;

namespace MindboxTask1Tests
{
    public class CircleTests
    {
        [Fact]
        public void Constructor_IncorrectRadius_ShouldThrowArgumentOutOfRangeException()
        {
            //Arrange

            //Act
            var func = () => { var circle = new Circle(-1.0); };

            //Assert
            func.Should().Throw<ArgumentOutOfRangeException>();
        }

        [Fact]
        public void Area_ShouldReturnCorrectArea()
        {
            //Arrange
            var circle = new Circle(1.0);

            //Act
            var area = circle.Area;

            //Assert
            area.Should().BeApproximately(Math.PI, 1e-6);
        }

    }
}